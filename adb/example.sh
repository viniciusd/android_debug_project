#!/bin/sh

for dev in $(adb devices | tail -n +2 | awk '{print $0}'); do
    adb -s ${dev} shell screencap -p /sdcard/sc.png
    adb -s ${dev} pull pull /sdcard/sc.png .

    adb -s ${dev} tcpip 5555
