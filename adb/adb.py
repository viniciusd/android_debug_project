from utils import run
from device import Device

class Adb:

    @staticmethod
    def version():
        return run("adb version")

    @staticmethod
    def devices():
        """ Returns a list of devices """
        devices = run("adb devices").splitlines()
        del devices[0]
        return [Device(serial) for serial in map(lambda dev: dev.split()[0], devices)]

    @staticmethod
    def connect(serial):
        run("adb connect "+serial)
        return Device(serial)
