from utils import run

class Device:

    def __init__(self, serial):
       self._serial = serial 

    def forward(self, host, android):
        """
        forward socket connection using:
            tcp:<port> (<local> may be "tcp:0" to pick any open port)
            localabstract:<unix domain socket name>
            localreserved:<unix domain socket name>
            localfilesystem:<unix domain socket name>
            dev:<character device name>
            jdwp:<process pid> (remote only)
        """
        run("adb -s {0} forward {1} {2}".format(self._serial, host, android))

    def forward_remove(self, serial=None):
        # if serial equals none, remove all
        if serial is None:
            run("adb foward --remove-all")
        else:
            run("adb foward --remove {}".format(serial))
        
    def reverse(self, android, host):
        """
        forward socket connection using:
            tcp:<port> (<local> may be "tcp:0" to pick any open port)
            localabstract:<unix domain socket name>
            localreserved:<unix domain socket name>
            localfilesystem:<unix domain socket name>
        """
        run("adb -s {0} reverse {1} {2}".format(self._serial, android, host))

    def reverse_remove(self, serial=None):
        # if serial equals none, remove all
        if serial is None:
            run("adb reverse --remove-all")
        else:
            run("adb reverse --remove {}".format(serial))

    def push(self, local, remote):
        run("adb -s {0} push {1} {2}".format(self._serial, local, remote))

    def pull(self, remote, local):
        run("adb -s {0} pull {1} {2}".format(self._serial, remote, local))

    def sync(self, directory=''):
        # If None, sync all
        run("adb -s {0} sync {1}".format(self._serial, directory))

    def shell(self, cmd=''):
        return run("adb -s {0} shell {1}".format(self._serial, cmd))

    def install(self, apk, cmd=''):
        return run("adb -s {0} install {1} {2}".format(self._serial, cmd, apk))

    def jdwp(self):
        return run("adb -s {0} jdwap".format(self._serial))

    def logcat(self, cmd=''):
        return run("adb -s {0} shell logcat -d {1}".format(self._serial, cmd))

    def state(self):
        return run("adb -s {} get-state".format(self._serial))

    def serial(self):
        return run("adb -s {} get-serialno".format(self._serial))

    def path(self):
        return run("adb -s {} get-devpath".format(self._serial))

    def remount(self):
        run("adb -s {} remount".format(self._serial))

    def reboot(self, optional=''):
        options = set('bootloader', 'recovery', '')
        if optional in options:
            run("adb -s {0} reboot {1}".format(self._serial, optional))
        else:
            raise ValueError("Invalid reboot mode. Choose 'bootloader, recovery or nothing'")

    def tcpip(self, port):
        try:
            run("adb -s {0} tcpip {1}".format(self._serial, port))
        except:
            raise ValueError("Problem to connect")

    def usb(self):
        run("adb -s {} usb".format(self._serial))

    def root(self):
        run("adb -s {} root".format(self._serial))

    def unroot(self):
        run("adb -s {} unroot".format(self._serial))

    def start_server(self):
        run("adb -s {} start-server".format(self._serial))

    def kill_server(self):
        run("adb -s {} kill-server".format(self._serial))

    def reconnect(self):
        run("adb -s {} reconnect".format(self._serial))

    def screenshot(self, path):
        run("adb -s {0} shell screencap -p {1}".format(self._serial, path))
