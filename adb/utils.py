from fabric.api import hide, local

__all__ = ['run']

def run(command):
    with hide('output', 'running'):
        result = local(command, capture=True)
    return result
